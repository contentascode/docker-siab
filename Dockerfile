From node:8

RUN apt-get update -qq && apt-get -qq -y install pandoc texlive texlive-latex-extra
RUN rm -rf /var/lib/apt/lists/*
RUN npm install -g grunt-cli

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.relative.json ./package.json
RUN npm install
